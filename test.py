import pypocketfft
import pyfftw
import numpy as np
import pytest

pmp = pytest.mark.parametrize

shapes1D = ((10,), (127,))
shapes2D = ((128, 128), (128, 129), (1, 129), (129,1))
shapes3D = ((32,17,39),)
shapes = shapes1D+shapes2D+shapes3D
len1D=range(1,2048)

def _l2error(a,b):
    return np.sqrt(np.sum(np.abs(a-b)**2)/np.sum(np.abs(a)**2))

@pmp("len", len1D)
def test1D(len):
    a=np.random.rand(len)-0.5 + 1j*np.random.rand(len)-0.5j
    b=a.astype(np.complex64)
    c=a.astype(np.complex256)
    assert(_l2error(a, pypocketfft.ifftn(pypocketfft.fftn(c))*np.float128(1.)/len)<1e-18)
    assert(_l2error(a, pypocketfft.ifftn(pypocketfft.fftn(a),fct=1./len))<1.5e-15)
    assert(_l2error(a.real, pypocketfft.irfftn(pypocketfft.rfftn(a.real),fct=1./len,lastsize=len))<1.5e-15)
    tmp=a.copy()
    assert (pypocketfft.ifftn(pypocketfft.fftn(tmp, inplace=True), fct=1./len, inplace=True) is tmp)
    assert(_l2error(tmp, a)<1.5e-15)
    assert(_l2error(b, pypocketfft.ifftn(pypocketfft.fftn(b),fct=1./len))<6e-7)
    assert(_l2error(b.real, pypocketfft.irfftn(pypocketfft.rfftn(b.real),fct=1./len,lastsize=len))<6e-7)
    tmp=b.copy()
    assert (pypocketfft.ifftn(pypocketfft.fftn(tmp, inplace=True), fct=1./len, inplace=True) is tmp)
    assert(_l2error(tmp, b)<6e-7)

@pmp("shp", shapes)
@pmp("nthreads", (0,1,2))
def test_fftn(shp, nthreads):
    a=np.random.rand(*shp)-0.5 + 1j*np.random.rand(*shp)-0.5j
    assert(_l2error(pyfftw.interfaces.numpy_fft.fftn(a), pypocketfft.fftn(a, nthreads=nthreads))<1e-15)
    a=a.astype(np.complex64)
    assert(_l2error(pyfftw.interfaces.numpy_fft.fftn(a), pypocketfft.fftn(a, nthreads=nthreads))<5e-7)

@pmp("shp", shapes2D)
@pmp("axes", ((0,),(1,),(0,1),(1,0)))
def test_fftn2D(shp, axes):
    a=np.random.rand(*shp)-0.5 + 1j*np.random.rand(*shp)-0.5j
    assert(_l2error(pyfftw.interfaces.numpy_fft.fftn(a, axes=axes), pypocketfft.fftn(a, axes=axes))<1e-15)
    a=a.astype(np.complex64)
    assert(_l2error(pyfftw.interfaces.numpy_fft.fftn(a, axes=axes), pypocketfft.fftn(a, axes=axes))<5e-7)

@pmp("shp", shapes)
def test_rfftn(shp):
    a=np.random.rand(*shp)-0.5
    assert(_l2error(pyfftw.interfaces.numpy_fft.rfftn(a), pypocketfft.rfftn(a))<1e-15)
    a=a.astype(np.float32)
    assert(_l2error(pyfftw.interfaces.numpy_fft.rfftn(a), pypocketfft.rfftn(a))<5e-7)

@pmp("shp", shapes)
def test_rfft_scipy(shp):
    for i in range(len(shp)):
        a=np.random.rand(*shp)-0.5
        assert(_l2error(pyfftw.interfaces.scipy_fftpack.rfft(a,axis=i), pypocketfft.rfft_scipy(a,axis=i))<1e-15)
        assert(_l2error(pyfftw.interfaces.scipy_fftpack.irfft(a,axis=i), pypocketfft.irfft_scipy(a,axis=i,fct=1./a.shape[i]))<1e-15)

@pmp("shp", shapes2D)
@pmp("axes", ((0,),(1,),(0,1),(1,0)))
def test_rfftn2D(shp, axes):
    a=np.random.rand(*shp)-0.5
    assert(_l2error(pyfftw.interfaces.numpy_fft.rfftn(a, axes=axes), pypocketfft.rfftn(a, axes=axes))<1e-15)
    a=a.astype(np.float32)
    assert(_l2error(pyfftw.interfaces.numpy_fft.rfftn(a, axes=axes), pypocketfft.rfftn(a, axes=axes))<5e-7)

@pmp("shp", shapes)
def test_identity(shp):
    a=np.random.rand(*shp)-0.5 + 1j*np.random.rand(*shp)-0.5j
    assert(_l2error(pypocketfft.ifftn(pypocketfft.fftn(a),fct=1./a.size), a)<1.5e-15)
    tmp=a.copy()
    assert (pypocketfft.ifftn(pypocketfft.fftn(tmp, inplace=True), fct=1./a.size, inplace=True) is tmp)
    assert(_l2error(tmp, a)<1.5e-15)
    a=a.astype(np.complex64)
    assert(_l2error(pypocketfft.ifftn(pypocketfft.fftn(a),fct=1./a.size), a)<6e-7)

@pmp("shp", shapes)
def test_identity_r(shp):
    a=np.random.rand(*shp)-0.5
    b=a.astype(np.float32)
    for ax in range(a.ndim):
        n = a.shape[ax]
        assert(_l2error(pypocketfft.irfftn(pypocketfft.rfftn(a,(ax,)),(ax,),lastsize=n,fct=1./n), a)<1e-15)
        assert(_l2error(pypocketfft.irfftn(pypocketfft.rfftn(b,(ax,)),(ax,),lastsize=n,fct=1./n), b)<5e-7)

@pmp("shp", shapes)
def test_identity_r2(shp):
    a=np.random.rand(*shp)-0.5 + 1j*np.random.rand(*shp)-0.5j
    a=pypocketfft.rfftn(pypocketfft.irfftn(a))
    fct = 1./pypocketfft.irfftn(a).size
    assert(_l2error(pypocketfft.rfftn(pypocketfft.irfftn(a),fct=fct), a)<1e-15)

@pmp("shp", shapes2D+shapes3D)
def test_hartley2(shp):
    a=np.random.rand(*shp)-0.5
    v1 = pypocketfft.hartley2(a)
    v2 = pypocketfft.fftn(a.astype(np.complex128))
    v2 = v2.real+v2.imag
    assert(_l2error(v1, v2)<1e-15)

@pmp("shp", shapes)
def test_hartley_identity(shp):
    a=np.random.rand(*shp)-0.5
    v1 = pypocketfft.hartley(pypocketfft.hartley(a))/a.size
    assert(_l2error(a, v1)<1e-15)

@pmp("shp", shapes)
def test_hartley2_identity(shp):
    a=np.random.rand(*shp)-0.5
    v1 = pypocketfft.hartley2(pypocketfft.hartley2(a))/a.size
    assert(_l2error(a, v1)<1e-15)
    v1 = a.copy()
    assert (pypocketfft.hartley2(pypocketfft.hartley2(v1, inplace=True), fct=1./a.size, inplace=True) is v1)
    assert(_l2error(a, v1)<1e-15)

@pmp("shp", shapes2D)
@pmp("axes", ((0,),(1,),(0,1),(1,0)))
def test_hartley2_2D(shp, axes):
    a=np.random.rand(*shp)-0.5
    fct = 1./np.prod(np.take(shp,axes))
    assert(_l2error(pypocketfft.hartley2(pypocketfft.hartley2(a, axes=axes), axes=axes, fct=fct),a)<1e-15)
