/*
 * This file is part of pocketfft.
 * Licensed under a 3-clause BSD style license - see LICENSE.md
 */

/*
 *  Python interface.
 *
 *  Copyright (C) 2019 Max-Planck-Society
 *  \author Martin Reinecke
 */

#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <pybind11/stl.h>

#include "pocketfft_hdronly.h"

//
// Python interface
//

namespace {

using namespace std;
using namespace pocketfft;

namespace py = pybind11;

auto c64 = py::dtype("complex64");
auto c128 = py::dtype("complex128");
auto c256 = py::dtype("complex256");
auto f32 = py::dtype("float32");
auto f64 = py::dtype("float64");
auto f128 = py::dtype("float128");

shape_t copy_shape(const py::array &arr)
  {
  shape_t res(size_t(arr.ndim()));
  for (size_t i=0; i<res.size(); ++i)
    res[i] = size_t(arr.shape(int(i)));
  return res;
  }

stride_t copy_strides(const py::array &arr)
  {
  stride_t res(size_t(arr.ndim()));
  for (size_t i=0; i<res.size(); ++i)
    res[i] = arr.strides(int(i));
  return res;
  }

shape_t makeaxes(const py::array &in, py::object axes)
  {
  if (axes.is(py::none()))
    {
    shape_t res(size_t(in.ndim()));
    for (size_t i=0; i<res.size(); ++i)
      res[i]=i;
    return res;
    }
  auto tmp=axes.cast<shape_t>();
  if ((tmp.size()>size_t(in.ndim())) || (tmp.size()==0))
    throw runtime_error("bad axes argument");
  for (auto sz: tmp)
    if (sz>=size_t(in.ndim()))
      throw runtime_error("invalid axis number");
  return tmp;
  }

#define DISPATCH(arr, T1, T2, T3, func, args) \
  auto dtype = arr.dtype(); \
  if (dtype.is(T1)) return func<double> args; \
  if (dtype.is(T2)) return func<float> args; \
  if (dtype.is(T3)) return func<long double> args; \
  throw runtime_error("unsupported data type");

template<typename T> py::array xfftn_internal(const py::array &in,
  const shape_t &axes, long double fct, bool inplace, bool fwd, size_t nthreads)
  {
  auto dims(copy_shape(in));
  py::array res = inplace ? in : py::array_t<complex<T>>(dims);
  auto s_in=copy_strides(in);
  auto s_out=copy_strides(res);
  auto d_in=reinterpret_cast<const complex<T> *>(in.data());
  auto d_out=reinterpret_cast<complex<T> *>(res.mutable_data());
  {
  py::gil_scoped_release release;
  c2c(dims, s_in, s_out, axes, fwd, d_in, d_out, T(fct), nthreads);
  }
  return res;
  }

py::array xfftn(const py::array &a, py::object axes, double fct, bool inplace,
  bool fwd, size_t nthreads)
  {
  DISPATCH(a, c128, c64, c256, xfftn_internal, (a, makeaxes(a, axes), fct,
           inplace, fwd, nthreads))
  }

py::array fftn(const py::array &a, py::object axes, double fct, bool inplace,
  size_t nthreads)
  { return xfftn(a, axes, fct, inplace, true, nthreads); }

py::array ifftn(const py::array &a, py::object axes, double fct, bool inplace,
  size_t nthreads)
  { return xfftn(a, axes, fct, inplace, false, nthreads); }

template<typename T> py::array rfftn_internal(const py::array &in,
  py::object axes_, long double fct, size_t nthreads)
  {
  auto axes = makeaxes(in, axes_);
  auto dims_in(copy_shape(in)), dims_out(dims_in);
  dims_out[axes.back()] = (dims_out[axes.back()]>>1)+1;
  py::array res = py::array_t<complex<T>>(dims_out);
  auto s_in=copy_strides(in);
  auto s_out=copy_strides(res);
  auto d_in=reinterpret_cast<const T *>(in.data());
  auto d_out=reinterpret_cast<complex<T> *>(res.mutable_data());
  {
  py::gil_scoped_release release;
  r2c(dims_in, s_in, s_out, axes, d_in, d_out, T(fct), nthreads);
  }
  return res;
  }

py::array rfftn(const py::array &in, py::object axes_, double fct,
  size_t nthreads)
  {
  DISPATCH(in, f64, f32, f128, rfftn_internal, (in, axes_, fct, nthreads))
  }

template<typename T> py::array xrfft_scipy(const py::array &in,
  size_t axis, long double fct, bool inplace, bool fwd, size_t nthreads)
  {
  auto dims(copy_shape(in));
  py::array res = inplace ? in : py::array_t<T>(dims);
  auto s_in=copy_strides(in);
  auto s_out=copy_strides(res);
  auto d_in=reinterpret_cast<const T *>(in.data());
  auto d_out=reinterpret_cast<T *>(res.mutable_data());
  {
  py::gil_scoped_release release;
  r2r_fftpack(dims, s_in, s_out, axis, fwd, d_in, d_out, T(fct), nthreads);
  }
  return res;
  }

py::array rfft_scipy(const py::array &in, size_t axis, double fct, bool inplace,
  size_t nthreads)
  {
  DISPATCH(in, f64, f32, f128, xrfft_scipy, (in, axis, fct, inplace, true,
    nthreads))
  }

py::array irfft_scipy(const py::array &in, size_t axis, double fct,
  bool inplace, size_t nthreads)
  {
  DISPATCH(in, f64, f32, f128, xrfft_scipy, (in, axis, fct, inplace, false,
    nthreads))
  }
template<typename T> py::array irfftn_internal(const py::array &in,
  py::object axes_, size_t lastsize, long double fct, size_t nthreads)
  {
  auto axes = makeaxes(in, axes_);
  size_t axis = axes.back();
  shape_t dims_in(copy_shape(in)), dims_out=dims_in;
  if (lastsize==0) lastsize=2*dims_in[axis]-1;
  if ((lastsize/2) + 1 != dims_in[axis])
    throw runtime_error("bad lastsize");
  dims_out[axis] = lastsize;
  py::array res = py::array_t<T>(dims_out);
  auto s_in=copy_strides(in);
  auto s_out=copy_strides(res);
  auto d_in=reinterpret_cast<const complex<T> *>(in.data());
  auto d_out=reinterpret_cast<T *>(res.mutable_data());
  {
  py::gil_scoped_release release;
  c2r(dims_out, s_in, s_out, axes, d_in, d_out, T(fct), nthreads);
  }
  return res;
  }

py::array irfftn(const py::array &in, py::object axes_, size_t lastsize,
  double fct, size_t nthreads)
  {
  DISPATCH(in, c128, c64, c256, irfftn_internal, (in, axes_, lastsize, fct,
    nthreads))
  }

template<typename T> py::array hartley_internal(const py::array &in,
  py::object axes_, long double fct, bool inplace, size_t nthreads)
  {
  auto dims(copy_shape(in));
  py::array res = inplace ? in : py::array_t<T>(dims);
  auto axes = makeaxes(in, axes_);
  auto s_in=copy_strides(in);
  auto s_out=copy_strides(res);
  auto d_in=reinterpret_cast<const T *>(in.data());
  auto d_out=reinterpret_cast<T *>(res.mutable_data());
  {
  py::gil_scoped_release release;
  r2r_hartley(dims, s_in, s_out, axes, d_in, d_out, T(fct), nthreads);
  }
  return res;
  }

py::array hartley(const py::array &in, py::object axes_, double fct,
  bool inplace, size_t nthreads)
  {
  DISPATCH(in, f64, f32, f128, hartley_internal, (in, axes_, fct, inplace,
    nthreads))
  }

template<typename T>py::array complex2hartley(const py::array &in,
  const py::array &tmp, py::object axes_, bool inplace)
  {
  using namespace pocketfft::detail;
  auto dims_out(copy_shape(in));
  py::array out = inplace ? in : py::array_t<T>(dims_out);
  ndarr<cmplx<T>> atmp(tmp.data(), copy_shape(tmp), copy_strides(tmp));
  ndarr<T> aout(out.mutable_data(), copy_shape(out), copy_strides(out));
  auto axes = makeaxes(in, axes_);
  {
  py::gil_scoped_release release;
  simple_iter<cmplx<T>> iin(atmp);
  rev_iter<T> iout(aout, axes);
  if (iin.remaining()!=iout.remaining()) throw runtime_error("oops");
  while(iin.remaining()>0)
    {
    auto v = atmp.get(iin.ofs());
    aout.set(iout.ofs(), v.r+v.i);
    aout.set(iout.rev_ofs(), v.r-v.i);
    iin.advance(); iout.advance();
    }
  }
  return out;
  }

py::array mycomplex2hartley(const py::array &in,
  const py::array &tmp, py::object axes_, bool inplace)
  {
  DISPATCH(in, f64, f32, f128, complex2hartley, (in, tmp, axes_, inplace))
  }

py::array hartley2(const py::array &in, py::object axes_, double fct,
  bool inplace, size_t nthreads)
  {
  return mycomplex2hartley(in, rfftn(in, axes_, fct, nthreads), axes_,
    inplace);
  }

const char *pypocketfft_DS = R"""(Fast Fourier and Hartley transforms.

This module supports
- single and double precision
- complex and real-valued transforms
- multi-dimensional transforms

For two- and higher-dimensional transforms the code will use SSE2 and AVX
vector instructions for faster execution if these are supported by the CPU and
were enabled during compilation.
)""";

const char *fftn_DS = R"""(
Performs a forward complex FFT.

Parameters
----------
a : numpy.ndarray (np.complex64 or np.complex128)
    The input data
axes : list of integers
    The axes along which the FFT is carried out.
    If not set, all axes will be transformed.
fct : float
    Normalization factor
inplace : bool
    if False, returns the result in a new array and leaves the input unchanged.
    if True, stores the result in the input array and returns a handle to it.
nthreads : int
    Number of threads to use. If 0, use the system default (typically governed
    by the `OMP_NUM_THREADS` environment variable).

Returns
-------
np.ndarray (same shape and data type as a)
    The transformed data.
)""";

const char *ifftn_DS = R"""(Performs a backward complex FFT.

Parameters
----------
a : numpy.ndarray (np.complex64 or np.complex128)
    The input data
axes : list of integers
    The axes along which the FFT is carried out.
    If not set, all axes will be transformed.
fct : float
    Normalization factor
inplace : bool
    if False, returns the result in a new array and leaves the input unchanged.
    if True, stores the result in the input array and returns a handle to it.
nthreads : int
    Number of threads to use. If 0, use the system default (typically governed
    by the `OMP_NUM_THREADS` environment variable).

Returns
-------
np.ndarray (same shape and data type as a)
    The transformed data
)""";

const char *rfftn_DS = R"""(Performs a forward real-valued FFT.

Parameters
----------
a : numpy.ndarray (np.float32 or np.float64)
    The input data
axes : list of integers
    The axes along which the FFT is carried out.
    If not set, all axes will be transformed in ascending order.
fct : float
    Normalization factor
nthreads : int
    Number of threads to use. If 0, use the system default (typically governed
    by the `OMP_NUM_THREADS` environment variable).

Returns
-------
np.ndarray (np.complex64 or np.complex128)
    The transformed data. The shape is identical to that of the input array,
    except for the axis that was transformed last. If the length of that axis
    was n on input, it is n//2+1 on output.
)""";

const char *rfft_scipy_DS = R"""(Performs a forward real-valued FFT.

Parameters
----------
a : numpy.ndarray (np.float32 or np.float64)
    The input data
axis : int
    The axis along which the FFT is carried out.
fct : float
    Normalization factor
inplace : bool
    if False, returns the result in a new array and leaves the input unchanged.
    if True, stores the result in the input array and returns a handle to it.
nthreads : int
    Number of threads to use. If 0, use the system default (typically governed
    by the `OMP_NUM_THREADS` environment variable).

Returns
-------
np.ndarray (np.float32 or np.float64)
    The transformed data. The shape is identical to that of the input array.
    Along the transformed axis, values are arranged in
    FFTPACK half-complex order, i.e. `a[0].re, a[1].re, a[1].im, a[2].re ...`.
)""";

const char *irfftn_DS = R"""(Performs a backward real-valued FFT.

Parameters
----------
a : numpy.ndarray (np.complex64 or np.complex128)
    The input data
axes : list of integers
    The axes along which the FFT is carried out.
    If not set, all axes will be transformed in ascending order.
lastsize : the output size of the last axis to be transformed.
    If the corresponding input axis has size n, this can be 2*n-2 or 2*n-1.
fct : float
    Normalization factor
nthreads : int
    Number of threads to use. If 0, use the system default (typically governed
    by the `OMP_NUM_THREADS` environment variable).

Returns
-------
np.ndarray (np.float32 or np.float64)
    The transformed data. The shape is identical to that of the input array,
    except for the axis that was transformed last, which has now `lastsize`
    entries.
)""";

const char *irfft_scipy_DS = R"""(Performs a backward real-valued FFT.

Parameters
----------
a : numpy.ndarray (np.float32 or np.float64)
    The input data. Along the transformed axis, values are expected in
    FFTPACK half-complex order, i.e. `a[0].re, a[1].re, a[1].im, a[2].re ...`.
axis : int
    The axis along which the FFT is carried out.
fct : float
    Normalization factor
inplace : bool
    if False, returns the result in a new array and leaves the input unchanged.
    if True, stores the result in the input array and returns a handle to it.
nthreads : int
    Number of threads to use. If 0, use the system default (typically governed
    by the `OMP_NUM_THREADS` environment variable).

Returns
-------
np.ndarray (np.float32 or np.float64)
    The transformed data. The shape is identical to that of the input array.
)""";

const char *hartley_DS = R"""(Performs a Hartley transform.
For every requested axis, a 1D forward Fourier transform is carried out,
and the sum of real and imaginary parts of the result is stored in the output
array.

Parameters
----------
a : numpy.ndarray (np.float32 or np.float64)
    The input data
axes : list of integers
    The axes along which the transform is carried out.
    If not set, all axes will be transformed.
fct : float
    Normalization factor
inplace : bool
    if False, returns the result in a new array and leaves the input unchanged.
    if True, stores the result in the input array and returns a handle to it.
nthreads : int
    Number of threads to use. If 0, use the system default (typically governed
    by the `OMP_NUM_THREADS` environment variable).

Returns
-------
np.ndarray (same shape and data type as a)
    The transformed data
)""";

} // unnamed namespace

PYBIND11_MODULE(pypocketfft, m)
  {
  using namespace pybind11::literals;

  m.doc() = pypocketfft_DS;
  m.def("fftn",&fftn, fftn_DS, "a"_a, "axes"_a=py::none(), "fct"_a=1.,
    "inplace"_a=false, "nthreads"_a=1);
  m.def("ifftn",&ifftn, ifftn_DS, "a"_a, "axes"_a=py::none(), "fct"_a=1.,
    "inplace"_a=false, "nthreads"_a=1);
  m.def("rfftn",&rfftn, rfftn_DS, "a"_a, "axes"_a=py::none(), "fct"_a=1.,
    "nthreads"_a=1);
  m.def("rfft_scipy",&rfft_scipy, rfft_scipy_DS, "a"_a, "axis"_a, "fct"_a=1.,
    "inplace"_a=false, "nthreads"_a=1);
  m.def("irfftn",&irfftn, irfftn_DS, "a"_a, "axes"_a=py::none(), "lastsize"_a=0,
    "fct"_a=1., "nthreads"_a=1);
  m.def("irfft_scipy",&irfft_scipy, irfft_scipy_DS, "a"_a, "axis"_a, "fct"_a=1.,
    "inplace"_a=false, "nthreads"_a=1);
  m.def("hartley",&hartley, hartley_DS, "a"_a, "axes"_a=py::none(), "fct"_a=1.,
    "inplace"_a=false, "nthreads"_a=1);
  m.def("hartley2",&hartley2, "a"_a, "axes"_a=py::none(), "fct"_a=1.,
    "inplace"_a=false, "nthreads"_a=1);
  m.def("complex2hartley",&mycomplex2hartley, "in"_a, "tmp"_a, "axes"_a,
    "inplace"_a=false);
  }
